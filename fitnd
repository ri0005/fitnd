#!/usr/bin/env perl
$|=1; # set autoflush on 

# script to automate the data-fitting process for n-d data:

# 1) make a function "fit_func.c" with your desired functional form, as given
#    in the first argument (in quotes!)

# 2) build the fitting routine with this function

# 3) run the routine, grab the coefficients

# requires: tail, grep, sed, cat, gcc, gnuplot, wc
# (all these should be available on a decent Linux box) 

# TODO : use inline::c instead of external gcc
# TODO : use perl instead of external commands (grep, sed etc.)

use Carp qw(confess);
my $tmp = $ENV{FITND_TMP} // '/tmp'; # tmp directory location

my $vb=defined $ENV{FITND_VB} ? 1: 0;

# first, check cmd line args
check_args();

# get the fit data file
$fit_datafile=$ARGV[1]; # NB global! (needed in gnuplot())

# make a copy of the data
make_copy();

# get the dimension of the data
$dimension=get_dimension(); 

print STDERR "Dimension $dimension\n";


$cachesize=100; # number of functions to cache
if($gnutype)
{
    $points=$gnutype;
    if($points=~/^points$/)
    {
        # NB you can override this by setting gnutype to "points ps <n>"
        # since the regexp looks for s at the end of points
        print STDERR "MAKING POINTS BIG\n";
        $points="points ps 4"
    }
}
else
{
    $points="lines";
}
if($fitgnutype)
{
    $fitpoints=$fitgnutype;
    if($fitpoints=~/^points$/)
    {
        # NB you can override this by setting gnutype to "points ps <n>"
        # since the regexp looks for s at the end of points
        print STDERR "MAKING POINTS BIG\n";
        $fitpoints="points ps 4"
    }
}
else
{
    $fitpoints=$points;
}

$errpoints="points ps 0.2";

print "Data dimension $dimension\n" if($vb);
if($dimension>0)
{
    set_filelocations();
    make_function();
    build_fitnd();
    run_fitnd();
}
exit(0);

############################################################
############################################################

sub set_filelocations
{
    # file locations
    mkdir("$tmp/fitndprogs");

    $fit2_location = (grep {-d $_} ($ENV{FITND_SRC},
                                    $ENV{HOME}.'/progs/rob',
                                    $ENV{HOME}.'/svn/perl/scripts/fitting'))[0];

    if(!defined $fit2_location)
    {
        print "Could not locate fitting source code\n"; 
        exit;
    }

    $fit2_prog         = "$tmp/fitndprogs/fitnd"; # generated executable location
    $fit_func_location = "$tmp/fit2-nd_fit_func.c"; # the fitting function
    $gnuplot_pltfile   = "$tmp/fit2-nd.cf.plt";
    $gnuplot_psfile    = "$tmp/fit2-nd.cf.ps";
    $fitcache          = "$tmp/fit2-nd.cache";
    $errfile           = "$tmp/fit2-err.cf.dat";
}

sub check_args
{
    $respoints=" points ps 1.0 ";
    undef($noerrors);
    my $i;
    $nognu=0;

    # show help if required
    help() if(!$ARGV[0] || !$ARGV[1]);

    $siglast=0; # default! last column is not sigma data
    for($i=0;$i<=$#ARGV;$i++)
    {
        # check for xn=abc where n is a number, abc is a name
        if($ARGV[$i]=~/x(\d+)\=(\S.*)/)
        {
            $labelhash{"x$1"}=$2; # store in hash keyed by xn
        }
        elsif($ARGV[$i]=~/y\=(\S.*)/)
        {
            $labelhash{"y"}=$1;
        }
        elsif($ARGV[$i]=~/xtitle=(\S.*)/)
        {
            $gnutitle=$1;
        }
        elsif($ARGV[$i]=~/^fix/)
        {
            $fixlow  = 1 if($ARGV[$i] eq 'fixlow'  || $ARGV[$i] eq 'fixends');
            $fixhigh = 1 if($ARGV[$i] eq 'fixhigh' || $ARGV[$i] eq 'fixends');
            $fixends = 1 if($ARGV[$i] eq 'fixends');
        }
        elsif($ARGV[$i]=~/^gnutype=(\S.*)/)
        {
            # type of gnuplot output, choose from lines, points, dots 
            $gnutype=$1;
            print STDERR "Set GNUTYPE (for fit *and* data) $gnutype\n";
        }
        elsif($ARGV[$i]=~/^nognu$/)
        {
            $nognu=1;
        }
        elsif($ARGV[$i]=~/^fitgnutype=(\S.*)/)
        {
            $fitgnutype=$1;
            print STDERR "Set GNUTYPE (for fit) $fitgnutype\n";
        }
        elsif($ARGV[$i]=~/^siglast$/)
        {
            $siglast=1;
            # this mean the last column of the data is the sigma 
            # on each point
        }
        elsif($ARGV[$i]=~/^extragnu=(\S.*)/)
        {
            $extratemp=$1;
            if($extratemp=~/3D_(.*)/)
            {
                # meant for 3d graphs only
                $extra3dgnu=$extra3dgnu.$1;
                print STDERR "Set extra 3d gnuplot stuff \"$1\"\n";
            }
            else
            {
                $extragnu=$extragnu.$extratemp;
                print STDERR "Set extra gnuplot stuff \"$extratemp\"\n";
            }
        }
        elsif($ARGV[$i]=~/gnuplot=(.*)/)
        {
            $gnuplot_extras=$1;
        }
        elsif($ARGV[$i]=~/fitg/)
        {
            $fitg=1;
        }
        elsif($ARGV[$i]=~/^noerr/)
        {
            $noerrors=1;
        }
    }
}

sub help
{
    print "fitnd

Usage:

fitnd \"function\" <filename>

\"function\" should be in the form of a C-type function, with the letters a-q used as free parameters which you name, ^ is a free free parameter (named automatically using r1,r2,r3 etc), s-z are dependent variables (you can use x0,x1,x2,y1,s1 etc. as well). cos,sin,exp,tan,pow are treated explicitly and not turned into free parameters. ax is turned into (a*x), but don't try to rely on this too much - a properly formatted function is best!

The <filename> is the name of the file that contains the data (s t u v w x y z)\

Before running, export the environment variable FITND_SRC to point to the directory where you have the fitnd script. 

Examples:

    fitnd \"^+^*x0\" file.dat

     Fits file.dat, which contains <x,y> data, to a linear function.

fitnd \"^+^*x0*^x1\" file2.dat 

     Fits file2.dat, which contains <x,y,z> data, to a function a+b*x*y.

fitnd \"^+exp(-0.5*pow((x0-^)/^,2.0)\" file2.dat 1.0 2.0 3.0 

     Gaussian fit, with vertical offset=1.0, mean=2.0 and variables=3.0.

Optional arguments:

fixlow, fixhigh, fixends : these weight the ends of the data files so that they are effectively fixed. 

gnutype=... choose from lines, points, dots, etc. to fix the gnuplot line type

nognu  - disables gnuplotting (useful for scripts/speed)

extragnu=\"...\" - any string given here is passed to gnuplot prior to the plot being made. Useful for scripting.

(There are other arguments, but I cannot remember what they do :)

PLEASE NOTE

The assumption is that your data have magnitudes AROUND 1.0 ... if you start using tiny
numbers I humbly suggest that you multiply them to make them larger, and take this into account
when making the fit, or use log space. Fitnd will not do this for you, learn to use
sed, grep, gawk etc., or indeed Perl, instead.


Relevant environment variables

FITND_SRC

The location of the fitnd source files. Usually this is the directory which
you cloned using git.

FITND_GNUPLOT

The contents of this are sent to gnuplot before the fit function is plotted.

FITND_COEFF_FORMAT

This is the sprintf-style format specifier used to output coefficients. 
Default is \"%g\".

FITND_TMP

Temporary directory location. Default it \"/tmp/\".

FITND_VB

If set to \"1\" then verbose output is logged to the screen. Default is \"0\".


";
    exit(0);
}
sub make_function
{
    my $func=$ARGV[0];
    my $fcount;
    my $i;
    my $indent='     ';
    
    # the functional form is in argument 1, written exactly as you'd expect
    # for a C function, on a single line (probably ;)
    print "Making function $func ... "if($vb);
    $funcin=$func;

    # open file and print out standard C functional form
    open(FP,">$fit_func_location");
    print FP "
\#include <math.h>
\#include \"fit2.h\"
\#include <stdio.h>
\#define MIN(A,B) ((A)<(B)?(A):(B))
\#define MAX(A,B) ((A)>(B)?(A):(B))
\#define MIN3(A,B,C) MIN((A),MIN((B),(C)))
\#define MAX3(A,B,C) MAX((A),MAX((B),(C)))
double fit_funcnd(double x,double p\[\],int np)
\{
$indent double z;\nint i=(int)(x+0.1);
";

    # turn the function input into this script into a C function
    # with a,b,c etc turned into p[1],p[2],p[3] etc.
    $fcount=1;
    $func=cap_func($func);

    my $rcount=1;

    # turn variables into x_data
    print "\n"if($vb);
    $func=~s/$/ /;

    # first convert x0,x1,x2 etc. into the proper columns
    # e.g. x0 = column 1, x1 = column 2 etc.
    for($i=0;$i<$dimension-1;$i++)
    {
        print "Checking for x$i\n"if($vb);
        if($func=~/(x)($i)/)
        {
            $change_let = $1;
            $change_num = $2;
            $col_num    = $change_num+1;
            print "Allocating $change_let$change_num <-> data column $col_num\n"if($vb);
            $func=~s/$change_let$change_num(\D)/X_DATA\[X_INDEX\($col_num,__I__\)\]$1/g;
            $transformhash{$col_num}="$change_let$i"; # so we can change back!
            $rcount++;
        }
    }
    print "Func now $func\n"if($vb);
    # change remaining free variables
    $func=~s/$/ /;

    while($func=~/([s-z]\d*)/)
    {
        print "Data Column $rcount <-> $1 \n"if($vb);
        $change_let=$1;
        $func=~s/$change_let(\D)/X_DATA\[X_INDEX\($rcount,__I__\)\]$1/g;
        $transformhash{$rcount}=$change_let; # so we can change back!
        $rcount++;
    }

    # check that rcount is now the same as dimension
    if($rcount!=$dimension)
    {
        print "Warning : you are fitting $rcount variables to $dimension-dimensional data! Are you *sure* you want to do this? \n"if($vb);
    }

    # change free (unnamed) parameters into useful coeffs
    while($func=~s/\^/COEFF\[$fcount\]/)
    {
        $fcount++;
    }

    # convert coefficients into internal format
    $func=~s/$/ /;
    while($func=~/([a-q]\d*)/)
    {
        print "Coefficient $1 <-> COEFF\[$fcount\]\n"if($vb);
        $change_let = $1;
        $func =~ s/$change_let(\D)/COEFF\[$fcount\]$1/g;
        $coefftransformhash{$fcount} = $change_let;
        $fcount++;
    }
    $func = "z=".reverse_func_cap($func);

    print "Function out $func \n"if($vb);
    $global_cfunc=$func; # save for later

    # now cast and close the function
    print FP $indent,casting($func),";
$indent return z;
\}
";
    close FP;

    $num_coeffs=$fcount-1;

    print "$num_coeffs coefficients)\n"if($vb);
}

sub build_fitnd
{
    my $command;
    $cf=cf_cache(); # nb global variable

    if($cf==-1)
    {
        update_cache();
        #print "Press return to build...\n";
        #<STDIN>;

        print "Building fitnd0 with given function ..."if($vb);
        $command="gcc -g -I$fit2_location -DNDIMENSIONAL -DEXTERNAL_FIT -Wstrict-prototypes -O3 -mtune=native -march=native $fit_func_location $fit2_location/nrutil.c $fit2_location/dcovstr.c $fit2_location/dgaussj.c $fit2_location/dlfit.c $fit2_location/mrqmin.c $fit2_location/fitnd.c -o ".$fit2_prog."0 -lc -lm 2>&1";
        print $command,"\n"if($vb);
        system($command);
        print "done\n"if($vb);
        $cf = 0;
    }
    else
    {
        print "Skipping build : using cached version\n"if($vb);
    }
}
sub cf_cache
{
    # compare the current function to the one in the cache
    my @cache=split(/\n/,`cat $fitcache 2> /dev/null`);
    my $newcache;
    my $i;
    my $ret=-1;
    
    $newcache=$global_cfunc;
    
    #print "$cache[1] c.f. $global_cfunc\n"if($vb);
    for($i=0;($i<=$cachesize);$i++)
    {
        #print "c.f. ",$cache[$i*2+1]," to $newcache\n"if($vb);
        if(($cache[$i*2+1] eq $newcache)&&
	   ((-s "$fit2_prog$i")>10))
        { # no need to rebuild - return the index number of the build we
          # should use
            print "Found function $i in cache, using $i\n"if($vb);
            $ret=$i;
            $i=$cachesize+2;
        }
        else
        { # need to rebuild?
            #print "We need to rebuild?\n"if($vb);
            $ret=-1;
        }
    }
    #print "return value $ret\n"if($vb);
    #return(-1); # enable this to force rebuilding
    return($ret);
}

sub update_cache
{
    my $size;
    my $func;
    my $c;
    my $cc;
    my $oldcache=`cat $fitcache`;
    # update the cache to the details for the current build
    # and current function

    # move the files
    for($i=$cachesize-1;$i>=0;$i--)
    {
        $ii=$i+1;
        rename "$fit2_prog$i","$fit2_prog$ii";
    }
    
    # up the numbers in the cache
    while($oldcache=~/FIT(\d+)\D/)
    {
        $c=$1;
        $cc=$c+1;
        $oldcache=~s/FIT$c /FID$cc /g;
    }
    $oldcache=~s/FID/FIT/g;

    # clean the stuff in the cache beyond the cachesize limit
    $oldcache=~s/FIT$cachesize.*\n(?:.*\n)*//;

    #print "Update cache : \n"if($vb);
    $func=$global_cfunc;
    #print "$size\n$func\n"if($vb);
    open (FP,">$fitcache");
    print FP "FIT0 \n$func\n",$oldcache;
    close FP;
}

sub run_fitnd
{
    my $command;
    my $res;
    my $j;
    
    print "Running fitnd ($fit2_prog$cf)\n"if($vb);
    $command="$fit2_prog$cf -f \"$tmp/fit.in.dat\" -coeffs $num_coeffs -coeffvals ".coeffs_string($num_coeffs)." -nd $dimension ";
    if($siglast==1)
    {
        $command=$command." -siglast";
    }
    print "RUN FIT COMMAND : \n",$command,"\n"if($vb);
    if($num_coeffs>0)
    {
        $res=`$command`;
    }
    else
    {
        print "Skipping fitnd because there are no free parameters\n"if($vb);
    }
    
    print "NUM COEFFS $num_coeffs\n"if($vb);
    if(get_coeffs($res)||($num_coeffs==0))
    {
        $res=~s/CHISQ.*\n//g;
        $res=~s/Split value number.*\n//g;
        $res=~s/Resetting to data_max.*\n//g;
        $res=~s/DumpC min.*\n//g;
        $res=~s/\n(First fit)/ : $1/g;
        $res=~s/\nD chi sq/ : delta(chisq)/g;
        $res=~s/final call.*\n//g;

        # use the transformhash to turn things back
        @keys=keys(%transformhash);
        print "Fit function 1\n$global_cfunc\n"if($vb);
        $orig_cfunc=$global_cfunc;
        foreach $key (@keys)
        {
            print "replacing xindex $key with $transformhash{$key}\n"if($vb);
            $global_cfunc=~s/x_data\[x_index\($key\,i\)\]/$transformhash{$key}/g;
        }
        print "Fit function 2\n$global_cfunc\n"if($vb);
        # do the same for the coefficients
        
        @keys=keys(%coefftransformhash);
        foreach $key (@keys)
        {
            $global_cfunc=~s/p\[$key\]/$coefftransformhash{$key}/g;
        }

        #transform remaining coefficients (which were free and unnamed) 
        # into r1, r2 etc.
        $rcount=0;
        while($global_cfunc=~/p\[(\d+)\]/)
        {
            $mat=$1;
            $rcount++;
            $global_cfunc=~s/p\[$mat\]/r$rcount/g;
        }
        print "Fit function 3\n$global_cfunc\n"if($vb);
        $global_cfunc=clean_func($global_cfunc);
        print "Fit function 4\n$global_cfunc\n"if($vb);
        print "Max error ",get_max_error_from_res($res),"\n";
        print "Mean error ",get_mean_error_from_res($res),"\n";
        print "RMS error ",get_rms_error($res),"\n";
        
        @coeffs = get_coeffs_from_res($res);
        if($num_coeffs>0)
        {
            print "Coeffs : @coeffs\n\n";
        }
        elsif($num_coeffs==0)
        {
            print "No coeffs : just dumping fit to gnuplot\n";
        }
        else
        {
            print "er... we seem to have a negative number of coefficients\n";
        }

	#print "\nCFUNC $orig_cfunc\n";
	$final_cfunc=$orig_cfunc;
	my $ci;
	for($ci=1;$ci<=1+$#coeffs;$ci++)
	{
	    $orig_cfunc=~s/p\[$ci\]/$coeffs[$ci]/g;
	}
	my @keys;
	my $key;
	@keys=keys(%transformhash);
	foreach $key (@keys)
        {
            #print "replacing xindex $key with $transformhash{$key}\n";
            $orig_cfunc=~s/x_data\[x_index\($key\,i\)\]/$transformhash{$key}/g;
        }
	$orig_cfunc=~s/x0/m/g;
	$orig_cfunc=~s/x1/z/g;

	$orig_cfunc=~s/--/+/g;
	print "\nCFUNC $orig_cfunc\n";


        # use gnuplot to produce output
      	gnuplot(@coeffs);
    }
    else
    {
        print STDERR "Fit failed, function \"$global_cfunc\", data \"$fit_datafile\"\n";
        print "Fit failed! DUMP:\n";
        print $res,"\n --- oops. fit finished in failure :(\n\n";
    }

}

sub coeffs_string
{
    my $i,$ret;
    my $count=0;
    for($i=0;$i<=$_[0];$i++)
    {
        if((!$ARGV[2+$i])||
           # if the coefficient has an "x" in it ignore it since it's a label
           ($ARGV[2+$i]=~/x/)||
           # also the other command line options (there must be a better way)
           ($ARGV[2+$i]=~/y/)||
           ($ARGV[2+$i]=~/gnu/)||
           ($ARGV[2+$i]=~/fitg/)||
           ($ARGV[2+$i]=~/extragnu/)||
           ($ARGV[2+$i]=~/siglast/)||
           ($ARGV[2+$i]=~/noerr/))
        {
            #$ret=$ret." ".1.0/($i+1);
	}
        else
        {
            # select the coeff value from the remaining arguments
	    $ret=$ret." ".$ARGV[$i+2];
	    $count++;
	}
    }

    for($i=$count;$i<$_[0];$i++)
    {
	$ret=$ret." ".1.0/($i+1);
    }

    return(" ".$ret." ");
}

sub get_coeffs
{
    # function to get coefficient values from a fit result
    my $data=$_[0];
    if(!$data)
    {
        # fatal error
        print "No data in get_coeffs()\n"if($vb);
        print "$data\n"if($vb);
        return("");
    }
    $data=~/.*\n/;
    while($data=~s/\s*(\d+)\s*\:\s*(\S+).*\n//)
    {
        $coeff[$1]=$2;
#        print "Got coeff $1 $coeff[$1]\n"if($vb);
    }
    return(@coeff);
}

sub gnuplot
{
    my $h1,$h2;
    my @coeffs=@_;
    my $gnufunc=$global_cfunc;
    # turn the c function to a gnuplot-style function
    $gnufunc=~s/z=//;
    $gnufunc=~s/$/ /;
    #$gnufunc=~s/pow\(([^,]+)\,([^)]+)\)/\(\($1\)\*\*\($2\)\)/g;
    print "global_cfunc $global_cfunc\n"if($vb);
    for($i=1;$i<=$#coeffs;$i++)
    {
        print "replace coeff $i (r$i) with $coeffs[$i] "if($vb);
        if($gnufunc=~s/r$i(\D)/\($coeffs[$i]\)$1/g)
        {
            print "OK\n"if($vb);
        }
        else
        {
            print "Failed!\n"if($vb);
        }
    }
    print "Transform hash : \n"if($vb);
    foreach $h1 (keys(%coefftransformhash))
    {
        $h2=$coefftransformhash{$h1};
        print "Transform $h1 -> $h2 -> $coeffs[$h1] (now $gnufunc)\n"if($vb);
        $gnufunc=~s/$h2/$coeffs[$h1]/g;

    }

    $pregnufunc=$gnufunc;
    $gnufunc=~s/x0/x/g;
    $gnufunc=~s/x1/y/g;

    print "GNUplot function $gnufunc\n";
    return if($nognu);

    my $plot = $dimension==3 ? "set zlabel \"z\"\n$extra3dgnu\nsplot" : 'plot';

    if($dimension<=3)
    {
        # we can only plot 2 or 3 dimensions
        $plotstring = "$extragnu\n$plot \"$fit_datafile\" title \"data\" with $points, $gnufunc title \"Fit\" with $fitpoints ";
    }
    open (GP,'>',$gnuplot_pltfile) || die("cannot open $gnuplot_pltfile (gnuplot script) for writing");
    print GP gnuplot_headers(),"\n$gnuplot_extras\nset title ";
    print GP $gnutitle ? "\"$gnutitle\"\n" : "\"Fit $fit_datafile to $gnufunc\"\n";
    print GP "set xlabel \"x\"\nset ylabel \"y\"\n$extragnu\n$plotstring\n\n";

    
    # for dimension >= 3 plot 2d plots for each variable
    if($dimension>=3)
    {

        if($dimension==3) 
        {
            print GP "\nset zlabel \"",$labelhash{"y"},"\"\n" if($labelhash{"y"});
            print GP "\nset view 30,30\n$plotstring\nset view 60,30\n$plotstring\nset view 60,200\n$plotstring\n";
        }

        # make comparison file
        print "Making cf file : $pregnufunc : $fit_datafile\n";
        
        $command = $num_coeffs==0 ? 
            "make_cf_data.pl \"$funcin\" \"$fit_datafile\" \"$dimension\" > \"$fit_datafile.cf.dat\"" : 
            "make_cf_data.pl \"$pregnufunc\" \"$fit_datafile\" \"$dimension\" > \"$fit_datafile.cf.dat\"";
 
        print "Command : \"",$command,"\"\n";
        `$command`;
        
        # get mean residual
        $mean_residual=get_mean_residual($fit_datafile.'.cf.dat');
        print "mean residual $mean_residual\nDim > 3 gnuplot activated\n";

        for($i=1;$i<$dimension;$i++)
        {
            print GP ($num_coeffs>0) ?
                "set y2tics\nset y2label \"%err / residual\"\n" : 
                "set y2tics\nset y2label \"Residual\"\n";
            print GP "set ylabel \"";
            
            if($labelhash{"y"})
            {
                print GP $labelhash{"y"};
                print "Set y label ",$labelhash{"y"}if($vb);
            }
            else
            {
                print GP "fit";
            }
            
            print GP ;
            $ii=$i-1;
            
            print GP "\"\nset xlabel \"",($labelhash{"x$ii"} // "x$ii"),"\"\n";
            print GP "set title \"fit $fit_datafile vs $pregnufunc\"\n" if(!$gnutitle);
            print GP "$extragnu\nplot \"$fit_datafile.cf.dat\" using $i:",$dimension," title \"data\" with $points,";
            print GP " \"$fit_datafile.cf.dat\"  using $i:",$dimension+1+$siglast," title \"fit\" with $points ,";
            
            if($num_coeffs>0 && !$noerrors)
            {
                # if num coeffs==0 then we don't really have a fit error - 
                # just the residuals are more useful
                print GP "\"$fit_datafile.cf.dat\" using $i:",$dimension+2+$siglast, " axes x1y2 title \"%err\"  with $errpoints, ";
            }
            print GP " \"$fit_datafile.cf.dat\" using $i:",$dimension+4+$siglast, " axes x1y2 title \"residual\"  with $respoints \n";
        }
    }
    close GP;

    $home=$ENV{"HOME"};
    print "GNUPLOT ",`pwd`, "$gnuplot_pltfile \n";
    `nice -n +19 gnuplot $gnuplot_pltfile`;
    print "See $gnuplot_psfile for fit results\n";
}

sub gnuplot_headers
{
    # nice headers for gnuplot that we'll use a lot

    my $maths = "
MIN(a,b) = (a < b) ? a : b
MIN3(a,b,c)=MIN(a,MIN(b,c))
MAX(a,b) = (a > b) ? a : b
MAX3(a,b,c) = MAX(a,MAX(b,c))
pow(a,b)=(a**b)
fabs(a)=abs(a)

                                                           ";
    
    return $fitg ? 
        "
set terminal postscript enhanced color solid \"Times-Roman\" 15 portrait size 7.5,11
set output \"$gnuplot_psfile\"
set nologscale xy
set nologscale y2
set key below 
$maths
set lmargin 3" :
        "
set terminal postscript enhanced color solid \"Times-Roman\" 20
set output \"$gnuplot_psfile\"
set nologscale xy
set nologscale y2
set key top outside box
$maths 
".($ENV{FITND_GNUPLOT} // '')."\n"
        ;
}


sub cap_func
{
    my $func=$_[0];

    # saves some functions by capitalizing them
    # remove z= from the beginning if it's there
    $func=~s/\s*z\s*\=\s*//;

    # turn functions to capitals so they're not treated as variables
    # NB can enter functions as capitals on command line since here we
    # ignore case
    $func=~s/exp/EXP/gi;
    $func=~s/(a?)tan(h?)/\U$1TAN\U$2/gi;
    $func=~s/(a?)sin(h?)/\U$1SIN\U$2/gi;
    $func=~s/(a?)cos(h?)/\U$1COS\U$2/gi;
    $func=~s/pow/POW/g;
    $func=~s/log/LOG/g;
    $func=~s/max/MAX/g;
    $func=~s/min/MIN/g;
    $func=~s/fabs/FABS/g;
    $func=~s/x_data/X_DATA/gi;
    $func=~s/x_index/X_INDEX/gi;
    $func=~s/(\d)e([\d\-\+])/$1E$2/g;
    # turn e.g. ax into a*x
    while($func=~/[\^a-z]\d*[\^a-zA-Z]\d*/)
    {
        $func=~s/([\^a-z]\d*)([\^a-zA-Z]\d*)/$1\*$2/g;
    }
    return($func);
}

sub reverse_func_cap
{
    # as cap_func, but reversed
    my $func=$_[0];
    $func=~s/COEFF/p/g; # turn coeffs into p[] array members
    $func=~s/POW/pow/g;
    $func=~s/EXP/exp/g;
    $func=~s/FABS/fabs/g;
    $func=~s/ACOS(H?)/acos\L$1/g;
    $func=~s/ASIN(H?)/asin\L$1/g;
    $func=~s/ATAN(H?)/atan\L$1/g;
    $func=~s/COS(H?)/cos\L$1/g;
    $func=~s/SIN(H?)/sin\L$1/g;
    $func=~s/TAN(H?)/tan\L$1/g;
    $func=~s/LOG/log/g; 
    $func=~s/X_DATA/x_data/g;
    $func=~s/X_INDEX/x_index/g;
    $func=~s/__I__/i/g;
    $func=~s/(\d)E([\d\-\+])/$1e$2/g;
    return($func);

}
sub get_dimension
{
    my $c,$count=1;
    open(FP,'<'."$tmp/fit.in.dat") || confess;
    while($c=<FP>)
    {
        if($c!~/^\#/)
        {
            # this is a data line, count the number of data items
            $c=~s/^\s*//g;
            $c=~s/\s*$//g;
            while($c=~s/\S+\s+//)
            {
                $count++;
            }
            close FP;

            # offset by siglast if the last value is supposed to be sigma
            # ("error" on each point) data
            return($count-$siglast);
        }
    }
    close FP; 
    return(0);
}

sub clean_func
{
    my $C=$_[0];
    # clean up numbers by removing pointless brackets and "--"
    # while($C=~/\(-?\d+\.\d+\)/)
    # {
    #     $C=~s/\((-?\d+\.\d+)\)/$1/g;
    # }

    $C=~s/\-\-/\+/g;
    $C=~s/\+\-/\-/g;
    return($C);
}

sub get_max_error_from_res
{
    my ($res) = @_;
    if($res=~/Maximum error \= (.*)/)
    {
        return $1;
    }
    else
    {
        print STDERR "Failed to get max error from fit results\n";
        return undef;
    }
}
sub get_mean_error_from_res
{
    my ($res) = (@_);
    if($res=~/Mean error \= (.*)/)
    {
        return $1;
    }
    else
    {
        print STDERR "Failed to get mean error from fit results\n";
        return undef;
    }
}
sub get_rms_error
{
    my ($res) = @_;
    if($res=~/RMS error \= (.*)/)
    {
        return $1;
    }
    else
    {
        print STDERR "Failed to get RMS error from fit results\n";
        return;
    }
}

sub get_mean_residual
{
    print STDERR "RESID trying to get mean residual from $_[0]\n";
    my $t=`tail $_[0]`;
    if($t=~/mean residual (\S+)/)
    {
        print STDERR "got it : $1\n";
        return $1;
    }
    else
    {
        print STDERR "failed\n";
        return "";
    }
}

sub get_fit_from_res
{
    my ($res) = (@_);
    return ($res=~/Fit function y=(.*)/) ? $1 : "";
}

sub get_coeffs_from_res
{
    my ($res) = (@_);
    my @coeff;
    my $format = $ENV{'FITND_COEFF_FORMAT'} // '%g';
    print "Trying to get coeffs \n";
    $res=~s/(.*\n)*Coefficients.*\n//;
    $res=~s/Maximum error.*\n(.*\n)*//;
    while($res=~s/\s*(\d+)\s+\:\s+(\S+)//)
    {
        $coeff[$1] = sprintf($format,$2);
    }
    return @coeff;
}

sub casting
{
    my $func=$_[0];

    print $func,"\n";
    $func=~s/z\=/z\=\(double\)\( /;
    $func=~s/$/\)/;
    $func=~s/pow/\(double\)pow/g;
    $func=~s/exp/\(double\)exp/g;
    $func=~s/(a?(?:cos|sin|tan)h?)/\(double\)$1/g;
    $func=~s/log/\(double\)log/g;
    $func=~s/x_data/\(double\)x_data/g;
    print $func,"\n";
    return($func);
}



sub make_copy
{
    print "Making copy of $fit_datafile...\n";
    if($fixends)
    {
        my $headline;
        my $brk = 0;
        if($fixlow == 1)
        {
            # fix low end : get the first line of data and repeat it 
            # to fix the low end of the data
            
            open(FP,'<',$fit_datafile)||confess;
            
            # get first line of data in $headline
            while($l=<FP> && !$brk && $l!~/^\#/ && $l!~/^\s+$/)
            {
                # data line
                $brk=1;
                chomp($l);
                $headline=$l;
            }
            close FP;
            
            open(FP,'>'."$tmp/fit.in.dat")||confess;
            my $i;
            my $lc=`wc -l $fit_datafile`;
            chomp($lc);
            for($i=0;$i<$lc;$i++)
            {
                print FP $headline,"\n";
            }
            close FP;
        }
        else
        {
            # touch the file to make sure we can write to it
            open(FP,'>',"$tmp/fit.in.dat")||confess;
            close FP;
        }

        # select only data lines
        `grep -v \\\# $fit_datafile | grep . |sed s/d/e/i >> $tmp/fit.in.dat`;
        if($fixhigh==1)
        {
            my $tailline=`tail -1 $tmp/fit.in.dat`;
            chomp($tailline);
            open(FP,">>$tmp/fit.in.dat")||confess;
            for($i=0;$i<$lc;$i++)
            {
                print FP $tailline,"\n";
            }
            close FP;
        }
    }
    else
    {
        `grep -v \\\# $fit_datafile | grep . |sed s/d/e/i > $tmp/fit.in.dat`;
    }
    print "done\n";
}
