#include "nrutil.h"
#include <stdio.h>
#ifdef DEBUG
#define MESS printf("OK at line %d\n",__LINE__);fflush(stdout);
#else
#define MESS //
#endif

/* Fitting function taken from page 673 of Numerical Recipies in C */
void lfit(double x[],
          double y[],
          double sig[],
          int ndat,
          double a[],
          int ia[],
          int ma,
          double **covar,
          double *chisq,
          void(*funcs)(double,double[],int))
{
    /*
     * Given a set of data points x[1..ndat],y[1..ndat] with indiviual standard
     * deviations sig[1..ndat] (set to 1 if you don't know them), use
     * chi-squared minimization to fit for some or all of the coefficients
     * a[1..ma] of a function that depends linearly on a, y=SUM(over i) a_i *
     * afunc_i (x). The input array ia[1..ma] indicates by nonzero entries
     * those components of a that should be fitted for, and by zero entires
     * those components that should be held fixed at their input values. The
     * program returns values for a[1..ma], chi-sqaured in chisq, and the
     * covariance matrix covar[1..ma][1..ma]. (Parameters held fixed will
     * return zero covariances.)
     */

    /*
     * The user supplies a routine funcs(x,afunc,ma) that returns the ma basis
     * functions evaluated at x=x in the array afunc[1..ma]
     */

    /* 16/01/2002 confirmed : this function works! */

    /* Prototypes, see functions with these names in this directory */
    void dcovsrt(double **covar,int ma, int ia[],int mfit);
    void dgaussj(double **a, int n,double **b, int m);

    /* Local Variables */
    int i,j,k,l,m,mfit=0;
    double ym,wt,sum,sig2i,**beta,*afunc;

    /* Code Block */
    beta = dmatrix(1,ma,1,1);
    afunc = dvector(1,ma);
    for(j=1;j<=ma;j++)
    {
        if(ia[j])
        {
            mfit++;
        }
    }
    if(mfit==0) nrerror("lfit: no parameters to be fitted");
#ifdef DEBUG
    printf("Size of covar matrix : %d x %d \n",sizeof(covar),sizeof(covar[0]));
#endif

    for(j=1;j<=mfit;j++)
    {
        for(k=1;k<=mfit;k++)
        {
#ifdef DEBUG
            printf("set covar %d,%d to zero ... ",j,k);fflush(stdout);
#endif
            covar[j][k]=0.0;
#ifdef DEBUG
            printf("OK\n");fflush(stdout);
#endif
        }
#ifdef DEBUG
        printf("set beta %d,1 to zero\n",j);fflush(stdout);
#endif
        beta[j][1]=0.0;
    }

    for(i=1;i<=ndat;i++)
    {
        /* Next page! p674 */
#ifdef DEBUG
        printf("lfit : Point %d, x=%f, y=%f\n",i,x[i],y[i]);
#endif
        (*funcs)(x[i],afunc,ma);
        ym=y[i];
        if(mfit<ma)
        {
            for(j=1;j<=ma;j++)
            {
                if(!ia[j])
                {
                    ym -= a[j]*afunc[j];
                }
            }
        }
        sig2i=1.0/SQR(sig[i]);
        for(j=0,l=1;l<=ma;l++)
        {
            if(ia[l])
            {
                wt=afunc[l]*sig2i;
                for(j++,k=0,m=1;m<=l;m++)
                {
                    if(ia[m])
                    {
                        covar[j][++k] += wt*afunc[m];
                    }
                }
                beta[j][1] += ym*wt;
            }
        }
    }

    for(j=2;j<=mfit;j++)
        for(k=1;k<j;k++)
            covar[k][j]=covar[j][k];
#ifdef DEBUG
    printf("Matrix solution... ");fflush(stdout);
#endif
    dgaussj(covar,mfit,beta,1); /* Matrix solution */
#ifdef DEBUG
    printf("OK\n");fflush(stdout);
#endif
    /* Partition solution (which means ?) to appropriate coefficients */
    for(j=0,l=1;l<=ma;l++)
    {
        if(ia[l])
        {
            a[l]=beta[++j][1];
        }
    }
    *chisq=0.0;

    /* Evaluate chi-squared */
    for(i=1;i<=ndat;i++)
    {
        (*funcs)(x[i],afunc,ma);
        for(sum=0.0,j=1;j<=ma;j++)
        {
            sum += a[j]*afunc[j];
        }
        *chisq += SQR((y[i]-sum)/sig[i]);
    }
    dcovsrt(covar,ma,ia,mfit);
    free_dvector(afunc,1,ma);
    free_dmatrix(beta,1,ma,1,1);

}
