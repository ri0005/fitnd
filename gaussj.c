#include <math.h>
#include "nrutil.h"
#include <stdio.h>
#define SWAP(a,b) {float temp=(a);(a)=(b);(b)=temp;}

void gaussj(float **a, int n,float **b, int m)
{

    int *indxc,*indxr,*ipiv;
    int i,icol=666,irow=0,j,k,l,ll;
    float big,dum,pivinv;
    //printf("In gaussj\n");
    fflush(stdout);

    indxc=ivector(1,n);
    indxr=ivector(1,n);
    ipiv=ivector(1,n);

    //printf("Alloced vectors\n");
    for (j=1;j<=n;j++)
    {
         for (k=1;k<=n;k++) {
                
             //printf("a[j=%d][k=%d]=%g\n",j,k,a[j][k]);
         }
    }
    fflush(stdout);

    for (j=1;j<=n;j++) ipiv[j]=0;

    //printf("heading into big loop\n");
    fflush(stdout);
    for (i=1;i<=n;i++) {
        big=0.0;
        //printf("set big\n");
        fflush(stdout);
        for (j=1;j<=n;j++)
            if (ipiv[j] != 1)
                for (k=1;k<=n;k++) {
                    //printf("k=%d\n",k);fflush(stdout);
                    if (ipiv[k] == 0) {
                        if (fabs(a[j][k]) >= big) {
                            //printf("OK\n");
                            fflush(stdout);
                            big=fabs(a[j][k]);
                            irow=j;
                            icol=k;
                        }
                        else
                        {
                            //printf("fabs too small (=%g c.f. big=%g)\n",
                            //fabs(a[j][k]),big);
                            fflush(stdout);
                        }
                    } else if (ipiv[k] > 1) nrerror("GAUSSJ: Singular Matrix-1");
                }
        //printf("out of k loop\n");fflush(stdout);
        //printf("icol=%d \n",icol);fflush(stdout);
        //printf("past first nerror, ipiv[icol=%d]=%d\n",icol,ipiv[icol]);
        fflush(stdout);

        ++(ipiv[icol]);

        //printf("Pre if\n");
        fflush(stdout);
        if (irow != icol) {
            for (l=1;l<=n;l++) SWAP(a[irow][l],a[icol][l])
                                   for (l=1;l<=m;l++) SWAP(b[irow][l],b[icol][l])
                                                          }
                
        //printf("past if\n");
        fflush(stdout);

        indxr[i]=irow;
        indxc[i]=icol;
        if (a[icol][icol] == 0.0) nrerror("GAUSSJ: Singular Matrix-2");
        pivinv=1.0/a[icol][icol];
        a[icol][icol]=1.0;
        for (l=1;l<=n;l++) a[icol][l] *= pivinv;
        for (l=1;l<=m;l++) b[icol][l] *= pivinv;
        for (ll=1;ll<=n;ll++)
            if (ll != icol) {
                dum=a[ll][icol];
                a[ll][icol]=0.0;
                for (l=1;l<=n;l++) a[ll][l] -= a[icol][l]*dum;
                for (l=1;l<=m;l++) b[ll][l] -= b[icol][l]*dum;
            }
    }

    //printf("near the end\n");
    fflush(stdout);
    for (l=n;l>=1;l--) {
        if (indxr[l] != indxc[l])
            for (k=1;k<=n;k++)
                SWAP(a[k][indxr[l]],a[k][indxc[l]]);
    }
    //printf("free vectors\n");fflush(stdout);
    free_ivector(ipiv,1,n);
    free_ivector(indxr,1,n);
    free_ivector(indxc,1,n);
    //printf("exit\n");fflush(stdout);
}

#undef SWAP
