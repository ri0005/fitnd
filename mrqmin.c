/*  mrqmin.c

    This is a roughly self-contained code module for
    Levenberg-Marquardt gradient descent iterative minimization
    of a nonlinear function.
    This started with the Numerical Recipes in C code

    Parts of this code Copr. 1986-92 Numerical Recipes Software 2.1.9-153.

    J. Watlington, 12/8/95

    Modified:
*/
//#define DEBUG
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "fit2.h"
#include "nrutil.h"



/*  mrqmin
    This is THE function.  If called with a negative alamda, it initializes.
    It should be called repeatedly without modifying alamda until it converges.
    Then it may be called one final time with alamda set to zero to obtain the
    final covariances.

    See NR in C, pp. 683 - 688 for real documentation, and don't forget to
    read the note about multi-dimensional use on p. 680.
*/

void mrqmin( double x[], double y[], double sig[], int ndata, double a[],
             int ia[], int ma, double **covar, double **alpha, double *chisq,
             void (*funcs)(double, double [], double *, double [], int),
             double *alamda)
{
    int j,k,l,m;
    static int mfit;
    static double ochisq,*atry,*beta,*da,**oneda;

    oneda = dmatrix(1,mfit,1,1); // sometimes we get a segfault if oneda is
    // not ALWAYS set to a matrix
    if(*alamda < 0.0)
    {
        atry = dvector(1,ma);
        beta = dvector(1,ma);
        da = dvector(1,ma);
        for(mfit=0,j=1;j<=ma;j++)
        {
            if(ia[j])
            {
                mfit++;
            }
        }
        oneda = dmatrix(1,mfit,1,1);
        *alamda=0.001;
        mrqcof(x,y,sig,ndata,a,ia,ma,alpha,beta,chisq,funcs);
        ochisq = *chisq;
        for(j=1;j<=ma;j++)
        {
            atry[j] = a[j];
        }
    }
    for(j=0,l=1;l<=ma;l++)
    {
        if(ia[l])
        {
            for(j++,k=0,m=1;m<=ma;m++)
            {
                if(ia[m])
                {
                    k++;
                    covar[j][k] = alpha[j][k];
#ifdef DEBUG
                    printf("Set covar[j=%d][k=%d] to %g from alpha[j][k]=%g\n",j,k,covar[j][k],alpha[j][k]);
#endif
                }
            }
            covar[j][j] = alpha[j][j] * (1.0 + *alamda);
#ifdef DEBUG
            printf("Set covar[j=%d][j=%d] to %g\n",j,j,covar[j][j]);
#endif
            oneda[j][1] = beta[j];
        }
    }
    dgaussj(covar,mfit,oneda,1);
#ifdef DEBUG
    printf("back in mrqmin\n");fflush(stdout);
    printf("coeffs now a1=%g, a2=%g\n",a[1],a[2]);
#endif
    for(j=1;j<=mfit;j++)
    {
        da[j]=oneda[j][1];
    }

    if(*alamda == 0.0)
    {
        dcovsrt(covar,ma,ia,mfit);
        fprintf(stderr,"Free matrix %p\n",oneda);
        free_dmatrix(oneda,1,mfit,1,1);
        free_dvector(da,1,ma);
        free_dvector(beta,1,ma);
        free_dvector(atry,1,ma);
        return;
    }
    for(j=0,l=1;l<=ma;l++)
    {
        if(ia[l])
        {
            atry[l]=a[l]+da[++j];
#ifdef DEBUG
            printf("setting atry[%d] with a[%d]=%g, da[%d]=%g to %g\n",
                   l,l,a[l],l,da[j],atry[l]);
#endif
        }
    }
#ifdef DEBUG
    printf("call mrqcof\n");fflush(stdout);
#endif
    mrqcof(x,y,sig,ndata,atry,ia,ma,covar,da,chisq,funcs);
#ifdef DEBUG
    printf("back from mrqcof\n");fflush(stdout);
#endif
    if(*chisq < ochisq) {
        *alamda *= 0.1;
        ochisq=(*chisq);
        for(j=0,l=1;l<=ma;l++)
        {
            if(ia[l])
            {
                for(j++,k=0,m=1;m<=ma;m++)
                {
                    if(ia[m])
                    {
                        k++;
                        alpha[j][k]=covar[j][k];
#ifdef DEBUG
                        printf("set alpha[j=%d][k=%d] to %g\n",j,k,alpha[j][k]);
                        fflush(stdout);
#endif
                    }
                }
                beta[j]=da[j];
                a[l]=atry[l];
            }
        }
    }
    else
    {
        *alamda *= 10.0;
        *chisq=ochisq;
    }
#ifdef DEBUG
    printf("leaving mrqmin\n");

    for(j=1;j<ma;j++)
    {
        printf("Coeff %d is %g\n",j,a[j]);
    }
    fflush(stdout);
#endif

}

/*   mrqcof
     Used by mrqmin to evaluate the linearized fitting matrix (alpha) and
     vector( beta) and calculate chi squared.
*/
void mrqcof(double x[], double y[], double sig[], int ndata, double a[],
            int ia[], int ma, double **alpha, double beta[], double *chisq,
            void (*funcs)(double, double [], double *, double [], int))
{
    int i,j,k,l,m,mfit=0;
    double ymod,wt,sig2i,dy,*dyda;
#ifdef MRQDEBUG
    printf("mrqcof x 0,1,2,3=%g,%g,%g,%g\n",x[0],x[1],x[2],x[3]);
#endif

    dyda=dvector(1,ma);
#ifdef MRQDEBUG
    printf("allocated vector dyda\n");fflush(stdout);
#endif

    for(j=1;j<=ma;j++)
    {
        if(ia[j])
        {
            mfit++;
        }
    }
    for(j=1;j<=mfit;j++)
    {
        for(k=1;k<=j;k++)
        {
            alpha[j][k]=0.0;
        }
        beta[j]=0.0;
    }
#ifdef MRQDEBUG
    printf("find chisq\n");fflush(stdout);
#endif

    *chisq=0.0;

    for(i=1;i<=ndata;i++)
    {
#ifdef DEBUG
        printf("CALLING FUNCS i=%d ndata=%d\n",i,ndata);
#endif

#ifdef NDIMENSIONAL
        /*
         * For n-dimensional fits, pass in i not x[i], and look up the data for x
         * in the fitting function itself
         */
        x[i]=i;
        (*funcs)((double)i,a,&ymod,dyda,ma);
#else
        (*funcs)(x[i],a,&ymod,dyda,ma);
#endif

#ifdef DEBUG
        printf("funcs gave y=%g\n",ymod);
#endif
        sig2i = 1.0 / (sig[i] * sig[i]);
        dy = y[i] - ymod;

        fprintf(stderr,"dy(i=%d) = %30.20e - %30.20e = %30.20e\n",i,y[i],ymod,dy);

#ifdef DEBUG
        printf("FUNCS got i=%d, ymod=%g, y=%g, dy=%g\n",
               i,ymod,y[i],dy);
#endif
        for(j=0,l=1;l<=ma;l++)
        {
            if(ia[l])
            {
#ifdef DEBUG
                printf("setting wt from  dyda[%d]=%g, sig2i=%g \n",
                       l,dyda[l],sig2i);
#endif
                wt = dyda[l]*sig2i;
                for(j++,k=0,m=1;m<=l;m++)
                {
                    if(ia[m])
                    {
#ifdef DEBUG
                        printf("Setting alpha[%d][%d] += wt=%g,dyda=%g\n",
                               j,k+1,wt,dyda[m]);
#endif
                        alpha[j][++k] += wt*dyda[m];
                    }
                }
                beta[j] += dy*wt;
            }
        }
        *chisq += dy*dy*sig2i;
        fprintf(stderr,"chisq -> %g\n",*chisq);
    }

    for(j=2;j<=mfit;j++)
    {
        for(k=1;k<j;k++)
        {
            alpha[k][j] = alpha[j][k];
        }
    }
    free_dvector(dyda,1,ma);
}
