/*  Include file for "fit2" */
#ifndef _FITTWO_H_
#define _FITTWO_H_

/*  Other include files we'll need */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/* Macros */
#define STRING_LENGTH 255
/* Comment out the next line for the non-debug version */
//#define DEBUG


#define MAX_REGIONS 10

#ifdef DEBUG
#define MESSAGE(A,B) fprintf(stderr,A,B);fflush(stderr);fflush(stdout);
#else
#define MESSAGE(A,B) /*  Nothing */
#endif /* DEBUG */

/** handy macros for calculating things **/
/** see Kernighan and Ritchie, 2nd ed Section 4.11, probably page 89 **/
#define MIN(A,B) ((A)<(B)?(A):(B))
#define MAX(A,B) ((A)>(B)?(A):(B))

/* function prototypes */

int load_data(void);
void split_data(void);
void parse_args(int argc,char ** argv);
void free_mem(void);
void lfit(double x[],double y[], double sig[], int ndat, double a[], int ia[],
          int ma, double **covar, double *chisq, void(*funcs)(double,double[],int));
void dcovsrt(double **covar, int ma, int ia[],int mfit);
void dgaussj(double **a, int n,double **b, int m);
void fitting_function(double x,
#ifdef THREED
                      double x2,
#endif
                      double a[],double *y, double *dyda, int na);
/******************************************************************************
 * Fitting functions
 */
#ifdef THREED
/* 3d fitting function */
#define FIT_FUNC_ARGS double x,double x2,double p[],int np
double fit_func3d(FIT_FUNC_ARGS);
#else
/* standard 2d fit */
#define FIT_FUNC_ARGS double x,double p[],int np
double fit_func(FIT_FUNC_ARGS);
#endif /* THREED */

#ifdef NDIMENSIONAL
/* use same args as standard 2d version of the function */
double fit_funcnd(FIT_FUNC_ARGS);
#endif

double fpoly(FIT_FUNC_ARGS);
double frat(FIT_FUNC_ARGS);
double ffermi(FIT_FUNC_ARGS);
double fpolyvar(FIT_FUNC_ARGS);

/******************************************************************************
 * End of fitting functions
 */
void dump_stat(int n,double *c,double chisq,double pcerr);
double calc_chisq(int n,
#ifndef NDIMENSIONAL
                 double *x,
#endif
#ifdef THREED
                 double *x2,
#endif
                 double *y, double *xsig,
                 double *coeffs,int nc);
void show_gnuplot_coeffs(double *coeffs,int num_coeffs);
double get_max_error(
    double *x_data,
    double *y_data,
    int datapoints,
    double *coeffs,
    int num_coeffs);

double get_mean_error(
    double *x_data,
    double *y_data,
    int datapoints,
    double *coeffs,
    int num_coeffs);


void help(void);
void fit_region(
#ifdef NDIMENSIONAL
    double *x_in,
#else
    double *x_in,
#endif

#ifdef THREED
                double *x2_in,
#endif
                double *y_in,
                int n); /* n=number of data points */
int separate_into_regions(double *x_data,
                           double *y_data,
                           const int datapoints);

#ifdef THREED
void mrqmin3d(double x[],double x2[],
              double y[], double sig[],double sig2[], int ndata, double a[],
              int ia[], int ma, double **covar, double **alpha, double *chisq,
              void (*funcs)(double, double , double[], double *, double [], int),
              double *alamda);
void mrqcof3d(double x[],double x2[], double y[], double sig[], double sig2[],
              int ndata, double a[],
              int ia[], int ma, double **alpha, double beta[], double *chisq,
              void (*funcs)(double, double ,double [], double *, double [], int));
#endif
void mrqcof(double x[], double y[], double sig[], int ndata, double a[],
            int ia[], int ma, double **alpha, double beta[], double *chisq,
            void (*funcs)(double, double [], double *, double [], int));
void mrqmin(double x[], double y[], double sig[], int ndata, double a[],
            int ia[], int ma, double **covar, double **alpha, double *chisq,
            void (*funcs)(double, double [], double *, double [], int),
            double *alamda);

/* Global variables */

/* Filename */
char filename[STRING_LENGTH];

/* Input data */
int datapoints; /* Number of lines of data */
double *y_data;
#ifdef THREED
double *x2_data,*x2_sig,*x2_temp;
#endif
#ifdef NDIMENSIONAL
double *x_data;
#else
double *x_data;
#endif

double *x_sig; /* Sigma for the x, usually 1 since we don't know*/
double *dydx;

/* temporary arrays */
double *x_temp,*y_temp;

int num_coeffs;
double *coeffs; /* NB double not double to be OK with NR routines */
int *coeff_bool; /* Switch coeff on or off (see lfit) */
int logoutput;
int manset_coeffs; /* is 1 if you manually set the initial values of the coefficients, 0 otherwise */


/* Stuff needed... */
double **fit_covar;
double fit_chisq; /* Chi-squared! */

/* Booleans for data handling */
int logx,logy;

/* Maximum percentage error of the fit */
double maxpcerr;
int using;/* gnuplot specific */

/* Flat regions... from flat_min to flat_max*/
int region_min[MAX_REGIONS];
int region_max[MAX_REGIONS];
double region_value[MAX_REGIONS]; /* Only of any value for flat regions */
int region_type[MAX_REGIONS];
/* A region is either flat, or to be fitted */
#define FLAT_REGION 0
#define FITTED_REGION 1

/* Define this to enable flat splitting */
#define FLAT_SPLITS

double flatness_threshold; /* Threshold fractional error for flatness */
/* Select threshold =0.00 to get just a normal fitd*/
double deriv_threshold; /* Sim for derivative selection */
int min_flat_length;

double split_values[MAX_REGIONS];
int num_split_values;
int split_manually;
double x_data_max;

/* C function stuff */
int c_out;
int f_out;
char elsif_string[6];
char elsif_string_f[6];

int dumpfits; /* 0 if we dump fits, 1 otherwise */

#define C_HEADER "C__"
#define C_INDENT "      "
#define C_HALF_INDENT "   "
/* Fortran function stuff */
#define F_HEADER "F__"
#define F_INDENT "      "
#define F_HALF_INDENT "      "
#define F_NEWLINE "F__     &"


void c_header(FILE *fp);
void c_footer(FILE *fp);
void dump_c_fit(FILE *fp,
                double *coeffs,int num_coeffs,
                double maxpcerr,
                double x_min,double x_max,int n,double chisq);


void f_footer(FILE *fp);
void f_header(FILE *fp);

void dump_f_fit(FILE *fp,
                double *coeffs,int num_coeffs,
                double maxpcerr,
                double x_min,double x_max,int n,double chisq);

void dump_c_flat(FILE *fp,double x_min,double x_max,double x_val);

void dump_f_flat(FILE *fp,double x_min,double x_max,double x_val);
int x_index(int a,int b);


int fitter; /* Type of function to fit with (default: polynomial) */
#define FIT_DEFAULT 2
#define FIT_POLY 0
#define FIT_POLY_VAR 1
#define FIT_POLY_RAT 2

char function_string[STRING_LENGTH];

#endif
