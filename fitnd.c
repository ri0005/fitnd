/* Debugging macros - handy! */
//#define DEBUG
//#define DERIV_DEBUG

/* Enable these on the command line with -DEXTERNAL_FIT -DNDIMENSIONAL */
//#define NDIMENSIONAL

/*
 * Always define EXTERNAL_FIT! It means we use an external fit function that is
 * not in this file! You can change this of course ;)
 */
#ifndef EXTERNAL_FIT
#define EXTERNAL_FIT
#endif
#include "fit2.h"
#include "nrutil.h"
#include <float.h>
#include <math.h>
/* Program to fit a set of data */


/* This is based on fit2.c, but is supposed to work on n-d data */

/* NB auto fitting has been removed */

/*
 * NOTE throughout this code, 1-based arrays are used to maintain compatibility
 * with Numerical Recipies. This annoys the hell out of me, but there you go
 */

static double get_rms_error(double *x_data,
                            double *y_data,
                            int datapoints,
                            double *coeffs,
                            int num_coeffs);

int num_d=0; /* number of dimensions */
double meanpcerr;
int sig1=0;
int main ( int argc, char * *  argv )
{
    /* Parse command line arguments */
    parse_args(argc, argv);

    /* Load in the data */
    load_data();

/* Fit all the data at once (split before entry to this program if you want) */
    if(datapoints>0)
    {
        /* Allocate memory and copy the required data */

        /* Fit */
        fit_region(x_data,y_data,datapoints-1);
    }

    /* Dummy return value */
    return(0);
}

void fit_region(double *x_in,
                double *y_in,
                int n) /* n=number of data points */
{
    /* Function to fig a region of data with a function  */
    double alamda;
    int i,chi_zero_count=0;
    double **fit_alpha;
    double old_chisq=1e10;
    double rmserr;;


    MESSAGE("Allocating memory : coeffs %d ... ",num_coeffs);
    if(manset_coeffs==0)
    {
        coeffs=malloc((num_coeffs+1)*sizeof(double)); /* NB DOUBLE not double */
    }
    MESSAGE("Again %s","");
    coeff_bool=malloc((num_coeffs+1)*sizeof(int));

    MESSAGE("Memory allocated OK %s","");
    /*
     * Tricky way to allocate a 2D array in C (I'm sure I can do it better!) NB
     * one-based arrays
     */
    fit_covar=(double **)calloc((num_coeffs+2),sizeof(double *));
    fit_covar[1]=(double *)calloc((num_coeffs+2)*(num_coeffs+2),sizeof(double));
    fit_alpha=(double **)calloc((num_coeffs+2),sizeof(double *));
    fit_alpha[1]=(double *)calloc((num_coeffs+2)*(num_coeffs+2),sizeof(double));

    for(i=2;i<=num_coeffs;i++)
    {
        fit_covar[i] = fit_covar[i-1] + num_coeffs;
        fit_alpha[i] = fit_alpha[i-1] + num_coeffs;
    }

#ifdef DEBUG
    printf("Size of covar = %zu x %zu\n",sizeof(fit_covar),sizeof(fit_covar[0]));
    printf("Size of alpha = %zu x %zu\n",sizeof(fit_covar),sizeof(fit_covar[0]));
#endif

    if((coeffs!=NULL)&&
       (coeff_bool!=NULL)&&
       (fit_covar!=NULL)&&
       (fit_alpha!=NULL))
    {
        MESSAGE("ok%s\n","");
#ifdef DEBUG
        printf("tried to malloc %zu bytes for fit_covar\n",
               ((num_coeffs+1)*(num_coeffs+1)*sizeof(double)));
#endif
        fflush(stdout);
    }
    else
    {
        perror("Failed to allocate memory");
    }


/* set all the coefficients to vary at once, you might want to change this */
    for(i=1;i<=num_coeffs;i++)
    {
        coeff_bool[i]=1; /* 1 to use this coeff, 0 to fix */
    }

    /*
     * Initial guesses for the coeffs if we haven't manually supplied them
     */
    if(manset_coeffs==0)
    {
        if((fitter==FIT_POLY)||
           (fitter==FIT_POLY_VAR))
        {
            for(i=1;i<=num_coeffs;i++)
            {
                coeffs[i]=(double)i;
            }
        }
        else if(fitter==FIT_POLY_RAT)
        {
            coeffs[1]=8.2;
            coeffs[2]=0.2;
        }
    }
    fflush(stdout);
    fflush(stderr);

    alamda=-1.0;  /* Initialize! */

    /* Call fitting function (lfit,mrqmin,whatever) */

    /* Call with alamda=-1.0 to init */
#ifdef DEBUG
    printf("mrqmin x_in 0,1,2,3=%g,%g,%g,%g\n",x_in[0],x_in[1],x_in[2],x_in[3]);
    //printf("Pre meqmin fit_alpha[2][3]=%g\n",fit_alpha[2][3]);
#endif
    maxpcerr = get_max_error(x_in,
                             y_in,
                             n,
                             coeffs,num_coeffs);

    dump_stat(num_coeffs,coeffs,
              calc_chisq(n,y_in,x_sig,coeffs,num_coeffs),
              maxpcerr);
    mrqmin(x_in,
           y_in,
           x_sig,
           n,
           coeffs,
           coeff_bool,
           num_coeffs,
           fit_covar,
           fit_alpha,
           &fit_chisq,
           fitting_function,
           &alamda);
    maxpcerr = get_max_error(x_in,
                             y_in,
                             n,
                             coeffs,num_coeffs);


    dump_stat(num_coeffs,coeffs,fit_chisq,maxpcerr);

#ifdef DEBUG
    printf("Back from first mrqmin call\n");fflush(stdout);
#endif
    /* Call a few times til we fit successfully */
#define MAX_ATTEMPTS 1000
    /* Where do we limit the attempts? unknown! but tis best to use a nice chiqs-type criterion... it's just I cannot think of a good one! */

/*
 * The maximum number of attempts we can have with (little or) no change in
 * chi-sqaured
 */
#define MAX_CHIZERO 30

    i=0;
    while(i < MAX_ATTEMPTS)
    {
        i++;

#ifdef DEBUG
        printf("Max error %g\n",maxpcerr);
        printf("Pr2e meqmin fit_alpha[2][3]=%g\n",fit_alpha[2][3]);
        printf("call \n");
#endif
        mrqmin(x_in,
               y_in,
               x_sig,
               n,
               coeffs,
               coeff_bool,
               num_coeffs,
               fit_covar,
               fit_alpha,
               &fit_chisq,
               fitting_function,
               &alamda);

        dump_stat(num_coeffs,
                  coeffs,
                  fit_chisq,
                  maxpcerr);
        maxpcerr = get_max_error(x_in,
                                 y_in,
                                 n,
                                 coeffs,num_coeffs);
        if(i>MAX_ATTEMPTS)
        {
            fprintf(stderr,"ARGH didn't converge within %d attempts :(\n",MAX_ATTEMPTS);
            break;
        }

        double dchisq = fabs(fit_chisq - old_chisq);
        fprintf(stderr,"D chi sq = %g (%d/%d) -> chisq = %g (/n = %g)\n",
                dchisq,
                chi_zero_count,
                MAX_CHIZERO,
                fit_chisq,
                fit_chisq/n);

        /*
         * If maximum percentage error < 1%, or we hit MAX_ATTEMPTS limit,
         * then dump out
         */
        fprintf(stderr,"dchisq %g / old_chisq %g = %g\n",
                dchisq,old_chisq,dchisq/old_chisq);
        if(old_chisq > 0.0 &&
           dchisq/old_chisq < 1e-2)
        {
            chi_zero_count++;
        }
        else
        {
            chi_zero_count = 0;
        }

        if(maxpcerr < 0.005 ||
           i > MAX_ATTEMPTS ||
           chi_zero_count>MAX_CHIZERO)
        {
            fprintf(stderr,
                    "BREAK : maxpcerr %g vs %g, i = %d vs MAX_ATTEMPTS %d, chi_zero_count = %d vs MAX_CHIZERO %d\n",
                    maxpcerr,0.005,
                    i,MAX_ATTEMPTS,
                    chi_zero_count,MAX_CHIZERO);
            break;
        }

        old_chisq = fit_chisq;
        fprintf(stderr,
                "oldchisq = %g\n",old_chisq);
    }
#undef MAX_ATTEMPTS
    printf("final call\n");
    alamda=0.0; /* Final call */
    mrqmin(x_in,
           y_in,
           x_sig,
           n,
           coeffs,
           coeff_bool,
           num_coeffs,
           fit_covar,
           fit_alpha,
           &fit_chisq,
           fitting_function,
           &alamda);
    maxpcerr = get_max_error(x_in,
                             y_in,
                             n,
                             coeffs,
                             num_coeffs);
    dump_stat(num_coeffs,
              coeffs,
              fit_chisq,
              maxpcerr);

    /* Now we should have our coeffs */

    /* Show what they are in gnuplot form */
    show_gnuplot_coeffs(coeffs,num_coeffs);

    /* Get the maximum error (percent) */

    maxpcerr = get_max_error(x_in,
                             y_in,
                             n,
                             coeffs,
                             num_coeffs);
    meanpcerr = get_mean_error(x_in,
                               y_in,
                               n,
                               coeffs,
                               num_coeffs);
    rmserr = get_rms_error(x_in,
                           y_in,
                           n,
                           coeffs,
                           num_coeffs);

    printf("Coefficients : \n");
    char * format = getenv("FITND_COEFF_FORMAT");
    for(i=1;i<=num_coeffs;i++)
    {
        printf(" %d : ",i);
        printf(format ? format : "%5.5g",
               coeffs[i]);
        printf(" \n");
    }
    printf("CHISQ = %f c.f points %d\n",fit_chisq,n);
    printf("Maximum error = %2.2f %%\n",maxpcerr);
    printf("Mean error = %2.2f %%\n",meanpcerr);
    printf("RMS error = %g\n",rmserr);

    /* Output a C function describing the fit */
    /* Later! */
}


double get_max_error(double *x_data,
                     double *y_data,
                     int datapoints,
                     double *coeffs,
                     int num_coeffs)
{
    /* Function to return the maximum percentage error */
    double maxpcerr = 0.0; /* Be small! i.e. zero */
    double err,pcerr;
    double calc_y;
    double y;
    int i;

    for(i=1;i<=datapoints;i++)
    {
        /* calc_y contains the value of the fitting function at x_data[i] */
        fitting_function((double)i,
                         coeffs,
                         &calc_y,
                         NULL,
                         num_coeffs);

        /* Got calc_y */
        y = y_data[i];

#ifdef DEBUG
        fprintf(stderr,"get max error calc_y=%g yreal=%g\n",calc_y,y);
#endif
        /* Cannot have y=0! This has no meaning. */
        if(fabs(y) > DBL_EPSILON)
        {
            err = y - calc_y;
            pcerr=100.0*fabs(err/y);
#ifdef DEBUG
            fprintf(stderr,"get max error : yreal=%g ycalc=%g pcerr=%2.2f\n",
                   y_data[i],calc_y,pcerr);
#endif
            if(pcerr > maxpcerr)
            {
                maxpcerr=pcerr;
            }
        }
    }

    return maxpcerr;
}

double get_mean_error(double *x_data,
                      double *y_data,
                      int datapoints,
                      double *coeffs,
                      int num_coeffs)
{
    /* Function to return the maximum percentage error */
    double meanpcerr=0.0; /* Be small! i.e. zero */
    double err,pcerr;
    double calc_y;
    double y;
    int i;

    for(i=1;i<=datapoints;i++)
    {
        /* calc_y contains the value of the fitting function at x_data[i] */
        fitting_function((double)i,coeffs,&calc_y,NULL,num_coeffs);

        /* Got calc_y */
        y=y_data[i];

#ifdef DEBUG
        printf("get max error calc_y=%g yreal=%g\n",calc_y,y);
#endif
        /* Cannot have y=0! This has no meaning. */
        if(fabs(y) > DBL_EPSILON)
        {
            err=y-calc_y;
            pcerr=100.0*fabs(err/y);
            meanpcerr+=pcerr;
        }
    }

    return(meanpcerr/datapoints);
}

static double get_rms_error(double *x_data,
                            double *y_data,
                            int datapoints,
                            double *coeffs,
                            int num_coeffs)
{
    /* Function to return the RMS error */
    double rmserr=0.0; /* Be small! i.e. zero */
    double calc_y;
    double y;
    int i;

    for(i=1;i<=datapoints;i++)
    {
        /* calc_y contains the value of the fitting function at x_data[i] */
        fitting_function((double)i,coeffs,&calc_y,NULL,num_coeffs);

        /* Got calc_y */
        y=y_data[i];
        rmserr+=(y-calc_y)*(y-calc_y);
    }
    rmserr/=(double)datapoints;
    rmserr=sqrt(rmserr);
    return(rmserr);
}
void show_gnuplot_coeffs(double *coeffs,int num_coeffs)
{
    char using_string[20];
    if(using==2)
    {
        strcpy(using_string," using 2:3 ");
    }
}

void fitting_function(double x,
                      double a[],double *y, double *dyda, int na)
{
    int i;
    double da,tda;
    static double (*ffunc)(double,double[],int);

/* Assign ffunc to be your particular fitting function */
    if(!ffunc)
    {
#ifndef EXTERNAL_FIT
#warning This version of fit2 ONLY uses external fits
        ffunc=fit_funcnd;
#else /* EXTERNAL_FIT */
        ffunc=fit_funcnd;

#ifdef DEBUG
        printf("Assigned external fitter\n");
#endif /* DEBUG */
#endif /* EXTERNAL_FIT */
    }

/* Get the value of y at this x */
#ifdef DEBUG
    printf("calling ffunc with x=%g a1=%g, a2=%g\n",x,a[1],a[2]);
#endif
    *y = ffunc(x,a,na);
#ifdef DEBUG
    printf("it gives y=%g\n",*y);
#endif
/* Numerically calculate the derivatives (hey, I wrote this bit ;) */
    if(dyda!=NULL)
    {

#ifdef DEBUG
        printf("Calc derivatives\n");fflush(stdout);
#endif
        for(i=1;i<=na;i++)
        {
            /* Calculate the derivatives using a centred difference approximation */
            //da=1e-1;

            da=1e-2;


#ifdef DERIV_DEBUG
            printf("Coeff i=%d, y=%g a=%g da=%g ... ",i,*y,a[i],da);
#endif
            /* Check zero condition! (very important) */
            if(fabs(da) < DBL_EPSILON)
            {
                da = 10.0 * DBL_EPSILON;
#ifdef DERIV_DEBUG
                printf("da was too small (now da=%g) ",da);
#endif
            }
            tda=2.0*da;
            a[i]+=da; /* Raise a by da */
            *(dyda+i)=ffunc(x,a,na);
            a[i]-=tda; /* Lower a by 2da to get da below where we started */
            dyda[i]-=ffunc(x,a,na);
#ifdef DERIV_DEBUG
            printf("numerator=%g denominator=%g ",dyda[i],tda);
#endif
            a[i]+=da; /* Back to where we started */
            dyda[i]/=tda;
#ifdef DERIV_DEBUG
            printf("dyda[%d]=%g\n",i,dyda[i]);
#endif
            /*
             * Check for infinities, zeroes, nans?
             * argh
             */
            if(dyda[i]>DBL_MAX) dyda[i]=DBL_MAX/20.0;
            if(dyda[i]<-DBL_MAX) dyda[i]=-DBL_MAX/20.0;
            if(fabs(dyda[i])<DBL_MIN) dyda[i]=20.0*DBL_MIN;
        }
#ifdef DEBUG
        //        printf("Fitting function return derivs : %g,%g,%g,%g\n",dyda[1],dyda[2],dyda[3],dyda[4]);
#endif
    }
#undef DERIV_DEBUG
}

/* Function to parse the command line arguments */
void parse_args(int argc,char ** argv)
{
    int i,j;
    char *arg;
    num_coeffs=3;
    fitter=FIT_DEFAULT;
    split_manually=0;
    dumpfits=0;

    flatness_threshold=0.10;
    deriv_threshold=0.9;
    min_flat_length=10;

    /* We haven't manually set the coeffs by default */
    manset_coeffs=0;
    coeffs=NULL;

/* Initialise C stuff */
    strcpy(elsif_string,"");
    strcpy(elsif_string_f,"");

    MESSAGE("Parse %d args\n",argc);

    for(i=1;i<argc;i++)
    {
        arg=*(argv+i);
        /* get number of coeffs first */
        if((strcmp(arg,"-coeffs"))==0)
        {
            num_coeffs=atoi(*(argv+(++i)));
        }
    }

    for(i=1;i<argc;i++)
    {
        MESSAGE("Arg %d ",i);
        arg=*(argv+i);
        MESSAGE("is \"%s\"\n",arg);

        if((strcmp(arg,"-f"))==0)
        {
            /* This is the filename for the data file */
            strncpy(filename,*(argv+i+1),(size_t)STRING_LENGTH);
            MESSAGE("Filename set to \"%s\"\n",filename);
            i++;
        }
        else if((strcmp(arg,"-coeffs"))==0)
        {
            i++; /* Ignore this argument */
        }
        else if((strcmp(arg,"-coeffvals"))==0)
        {
            /* Allocate memory for coeffs */
            coeffs = malloc((num_coeffs+1)*sizeof(double)); /* NB DOUBLE not double */
            /* set their values */
            for(j=1;j<=num_coeffs;j++)
            {
                coeffs[j]=atof(*(argv+i+j));
            }
            i+=j-1;
            manset_coeffs=1;
        }
        else if((strcmp(arg,"-nd"))==0)
        {
            i++;
            num_d=atoi(*(argv+i));
            //printf("Number of dimensions : %d\n",num_d);
        }
        else if((strcmp(arg,"-nofits"))==0)
        {
            dumpfits=1;
        }
        else if ((strcmp(arg,"-siglast"))==0)
        {
            sig1=1;
        }
        else if((strcmp(arg,"--help"))==0)
        {
            /* Show help text */
            help();
            exit(0);
        }
        else
        {
            fprintf(stderr,"Unknown argument \"%s\"\n",
                    arg);
            exit(-1);
        }
    }
}
void help(void)
{
    /* Show help text */
    fprintf(stdout,"Fitting Routine (based on Numerical Recipies lfit)\n\nArguments : \n\n-coeffs <n> ... use n coefficients in the fitting function\n-f <filename> ... use data in file <filename>\n\n-fitter <n> ... fit according to function type <n>, see fit1.h\n\n");
}

/*  Function to load the data points */
int load_data()
{
    //double dx,dy;
    int i;
    FILE *fp;
    int count=1;
    int comment_count=0;
    char c[STRING_LENGTH];
    int d;
    char *p;
    //char *s;
    MESSAGE("Loading data for file \"%s\"\n",filename);

    /*  1 Count the number of lines */
    fp=fopen(filename,"r");
    if(fp==NULL)
    {
        perror("Couldn't open input file");
        exit(-1);
    }

    MESSAGE("Counting lines in %s\n",filename);
    while(fgets(c,STRING_LENGTH,fp)!=NULL)
    {
        if((c[0]!='#')&&(strlen(c)>2))
        {
            count++;

        }
        else
        {
            //fprintf(stderr,"Bad line comment count %d\n",comment_count);
            comment_count++;
        }
    }
    //fprintf(stderr,"COUNT %d\n",count);
    MESSAGE("%d of them (remember -1!)",count);
    MESSAGE(" (%d comment(s))\n",comment_count);
    datapoints=count;

    /* 2 Allocate memory */
    /* Calloc or malloc? */
    MESSAGE("Allocating %zu bytes memory for input data... ", 2 * count*sizeof(double));
    /* x_data is a multidimensional array, indexed by the x_index function */
    x_data = malloc((num_d + 1)*(datapoints + 10) * sizeof(double));
    y_data = malloc((datapoints+10) * sizeof(double));
    x_sig  = malloc((datapoints+3) * sizeof(double));

    /* Set xsig to 1 everywhere (may be overridden below) */
    for(i=0;i<datapoints+3;i++)
    {
        x_sig[i]=1.0;
    }

    if(x_data != NULL &&
       y_data != NULL &&
       x_sig != NULL)
    {
        MESSAGE("OK%s","\n");
    }
    else
    {
        MESSAGE("Malloc FAILED!%s","\n");
        exit(-2);
    }

    MESSAGE("Closing file %s...",filename);
    /* Close the file */
    fclose(fp);

    /* 3 Load in the data */
    MESSAGE("reopening file %s\n",filename);
    fp=fopen(filename,"r");
    if(fp==NULL)
    {
        perror("Couldn't open input file");
        exit(-1);
    }

    /* reset the counter */
    count=0;
    while(fgets(c,STRING_LENGTH,fp)!=NULL)
    {
        if(c[0] != '#' && strlen(c) > 2)
            /* Ignore comment lines starting with # and blank lines (strlen<2) */

        {
            count++; /* count is the current data line */
            d = 0; /* d is the dimension being read in */
            p = strtok(c," ");

            while(p!=NULL)
            {
                d++;
                if(d < num_d)
                {
                    /* This is some x data */
                    *(x_data+x_index(d,count)) = atof(p);
#ifdef DEBUG
                    printf("setting xpoint (%d,%d) to %g\n",
                           d,
                           count,
                           *(x_data + x_index(d, count)));
#endif
                }
                else if(d == num_d)
                {
                    /* This is the final point, so is the y data */
                    *(y_data+count) = atof(p);
#ifdef DEBUG
                    printf("setting y point number %d with %g\n",
                           count,
                           *(y_data + count));
#endif
                }
                else if(d==num_d+1)
                {
                    if(sig1==1)
                    {
                        /*
                         * this is sigma data, the error on each point.
                         * Ordinarily sig1 is set to 0 and all points are
                         * equally biased, however you may with to change this
                         * : so the x_sig array can be set here
                         */
#ifdef DEBUG
                        fprintf(stderr,"Setting x sig %d to 1/%g\n",count,1.0/atof(p));
#endif
                        *(x_sig+count) = atof(p);
                    }
                }

                /* get next token... */
                p=strtok(NULL," ");
            }
            fprintf(stderr,"Loading data line number %d x0=%30.20e y=%30.20e\n",
                    count,
                    *(x_data+x_index(1,count)),
                    *(y_data+count));

            /* sscanf failed : do nothing! probably the final line */
        }
    }

    /*
     * Set the zero point to the same as the first point, and the same for the
     * final point
     */
    MESSAGE("OK - got data - %d lines of it ... ",count);
    MESSAGE("Closing file %s\n",filename);
    fclose(fp);

    MESSAGE("Returning count = %d\n",count-1);
    fflush(stdout);

    return count - 1;
}


double frac_error(double *data,int i, int j)
{
    return fabs(1.0 - data[j]) / data[i];
}

void dump_stat(int n,double *c,double chisq,double pcerr)
{
    /* Output something about the current status of the fitting */
    int i;
    printf("Fit gives new coeffs : ");
    for(i=1;i<=n;i++)
    {
        printf(" %d=%g",i,c[i]);
    }
    printf(" with chisq = %g, max %% error = %g\n",chisq,pcerr);
}

double calc_chisq(int n, double *y, double *xsig,
                  double *coeffs,int nc)
{
    /* Calculate the chi-squared of the current fit */
    int i;
    double dy,cs=0.0,yfit;
    for(i=1;i<=n;i++)
    {
        fitting_function((double)i,coeffs,&yfit,NULL,nc);
        dy=y[i]-yfit;
        cs+=dy*dy/xsig[i];

//#ifdef DEBUG
        fprintf(stderr,
                "CALC_CHISQ at i=%d, ydata=%g fit is %g cs+=%g now %g\n",
                i,y[i],yfit,dy*dy/xsig[i],cs);
//#endif
    }
    return(cs);
}

int x_index(int a,int b)
{
    extern int datapoints;
    /*
     * return the index number of an x point
     * a=dimension
     * b=point number
     */
    return a * datapoints + b;
}
