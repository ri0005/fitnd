#!/usr/bin/env perl

# read in a data file, and a fitting function, then output the same data file
# with the fit data, and % errors

# fitting function is arg0, in the form a*x0+b*x1 etc. i.e. in a form that
# we can "eval"
$func=$ARGV[0];

# data file
$file=$ARGV[1];


# new: override dimension with the dimension from the fitting routine
$dimension=$ARGV[2];
chomp($dimension);

print STDERR "MAKE CF DATA says dimension = $dimension, args are \n$ARGV[0]\n$ARGV[1]\n$ARGV[2]\n";

# open the file
open (FP,"<$file");

print  "# c.f. func = \"$func\" file = \"$file\" dim = \"$dim\"\n";
$linecount=0;
while($f=<FP>)
{
    # store the data we *had* so we can output it
    $linewas=$f;
    chomp($linewas);

    # get the data in the data line
    if(length($linewas)<2)
    {
        # blank line
        print "\n";
    }
    else
    {
        if(!($f=~/^\#/))
        {
            $count=0;
            $f=~s/^\s*//g;
            $f=~s/\s*$//g;
            while($f=~s/(\S+)\s*//)
            {
                $data[$count]=$1;
                $count++;
            }
        }
        if(!$dimension) 
        {
            $dimension=$count;
            #print "assuming dimension $dimension\n";
        }
        else 
        {
            if($count!=$dimension)
            {
                if(!$ARGV[2])
                {
                    print STDERR "make_cf_data : Warning : data count ($count) != dimension ($dimension) ! data corrupt? \n";
                }
                else
                {
                    # the last value is the sigma, so just use the overridden value 

                    $count=$dimension;
                    #print STDERR "make_cf_data: Warning count ($count) != dimension ($dimension) but I think this is ok because you have enabled siglast\n";
                }
           }
        }
        #print "Dim $dimension count $count\n";

        
        $tfunc=$func; #temp copy of the function
        for($i=0;$i<=$count;$i++)
        {
            # substitute these values in the fitting function
            $tfunc=~s/x$i/\($data[$i]\)/g;
        }
        $linecount++;

        # fix pows
#        while($tfunc=~/pow/)
#        {
#            $tfunc=~s/pow\(([^,]+)\,([^\)]+)\)/\($1\*\*$2\)/g;
#        }

        # fix --
        while($tfunc=~/\-\-/)
        {
            $tfunc=~s/\-\-/\+/g;
        }
        # eval the fitting function
        $result="";
	$result=eval $tfunc;
        #print STDERR "Tried to evaluate $tfunc -> got $result\n";

        if($result eq "")
        {
            print STDERR "WARNING: no result in make_cf_data.pl\n";
	    print STDERR "FUNC $tfunc\n";
	    print STDERR" RESULT $result\n";
	}

        $count--;
        $residual=($data[$count]-$result);
        $residual_sum+=$residual;
        $residual_rms+=abs($residual);
        $rmscount++;

        $rcount++;
#print "DATA is $data[$count] vs RESULT $result gives RESIDUAL $residual\n";        
 
# calculate the error
#        $count--; # save us calculating $count-1 lots of times
        if(abs($data[$count])>0)
        {
            $err=abs($residual/$data[$count])*100.0;

#print "# CALC ER res/data = $residual / $data[$count] = $err\n";
            $errsum+=$err;
            $ecount++;
        }
        else
        {
            $err=0.0;
        }
        $chisq+=($data[$count]-$result)**2;
        print "$linewas $result $err ",sprintf("%5.5e",$chisq)," ",$residual,"\n";
 #       print "#$linewas --< r->$result e->$err c->",sprintf("%5.5e",$chisq)," res->$residual\n";
    }
}
print "#errmean ",$errsum/$ecount,"\n";

print "# mean residual ",$residual_sum/$rcount,"\n";
print "# RMS residual ",$residual_rms/sqrt($rmscount),"\n";



# close file and exit
close FP;
exit(0);
sub log10 
{
    my $n = shift;
    return log($n)/log(10);
}
sub MAX
{
    if($_[0]>$_[1])
    {
	return($_[0]);
    }
    else
    {
	return($_[1]);
    }
}
sub MIN
{
    if($_[0]<$_[1])
    {
	return($_[0]);
    }
    else
    {
	return($_[1]);
    }
}
sub pow
{
    return($_[0]**$_[1]);
}
