_fitnd_ is a script which builds a modified version of the _Numerical Recipes_ Levenberg–Marquardt algorithm
to fit data to a function, all specified on the command line.

Before running, export the environment variable `FITND_SRC` to point to the directory where you have the fitnd script. 

Help is given below. 

You can test that things work by running, from your cloned directory, 

``
fitnd "^+^*x0+^*x0*x0+^*x0*x0*x0" "test.dat"
``

which should give you the output as in _fit-test-cubic.png_

Requirements: _cat_, _gcc_, _gnuplot_, _grep_, _nice_, _perl_, _sed_, _tail_, _wc_ 

For more help, run (from the `FITND_SRC` directory) fitnd without arguments. 



--------------------------------------------------------------------


_fitnd_

**Usage:**

``
fitnd "function" filename [coefficients] [other options]
``

"function" should be in the form of a C-type function, with the letters `a-q` used as free parameters which you name, `^` is a free parameter (named automatically using `r1`,`r2`,`r3` etc), `s-z` are dependent variables (you can use `x0`,`x1`,`x2`,... etc. as well). `cos`,`sin`,`exp`,`tan`,`pow` are treated explicitly and not turned into free parameters. `ax` is turned into `(a*x)`, but don't try to rely on this too much - a properly formatted function is best! Remember this is C, so use `pow(x,y)` rather than `x**y`.

The filename is the name of the file that contains the data. This should be in space-separated columns.

Before running, export the environment variable FITND_SRC to point to the directory where you have the fitnd script. This is required so the compilation step finds the various source files.

Examples:

``
    fitnd "^+^*x0" file.dat
``

Fits file.dat, which contains `<x,y>` data, to a linear function.

``
     fitnd "^+^*x0*^x1" file2.dat 
``

Fits file2.dat, which contains <x,y,z> data, to a function `a+b*x*y`.

``
     fitnd "^+exp(-0.5*pow((x0-^)/^,2.0)" file2.dat 1.0 2.0 3.0 
``

Gaussian fit, with vertical offset=1.0, mean=2.0 and variables=3.0.

**Optional arguments:**

`fixlow`, `fixhigh`, `fixends` : these weight the ends of the data files so that they are effectively fixed. 

`gnutype=...` choose from lines, points, dots, etc. to fix the _gnuplot_ line type

`nognu`  - disables gnuplotting (useful for scripts/speed)

`extragnu="..."` - any string given here is passed to gnuplot prior to the plot being made. Useful for scripting.

(There are other arguments, but I cannot remember what they do :)

**PLEASE NOTE**

The assumption is that your data have magnitudes _around_ 1.0 ... if you start using tiny
numbers I humbly suggest that you multiply them to make them larger, and take this into account
when making the fit, or use log space. Fitnd will not do this for you, learn to use
_sed_, _grep_, _gawk_ etc., or indeed `Perl`, instead.


Relevant environment variables

`FITND_SRC`

The location of the fitnd source files. Usually this is the directory which
you cloned using git.

`FITND_GNUPLOT`

The contents of this are sent to gnuplot before the fit function is plotted.

`FITND_COEFF_FORMAT`

This is the sprintf-style format specifier used to output coefficients. 
Default is `"%g"`.

`FITND_TMP`

Temporary directory location. Default it "/tmp/".

`FITND_VB`

If set to "1" then verbose output is logged to the screen. Default is "0".


